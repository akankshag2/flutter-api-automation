import 'api_tests/posts.test.dart' as post_tests;
import 'api_tests/comments.test.dart' as comment_tests;

void main() {
  post_tests.main();
  comment_tests.main();
}
