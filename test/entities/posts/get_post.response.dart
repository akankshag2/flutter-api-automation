import '../../model/post.model.dart';

class GetPostResponse {
  int statusCode;
  Post body;

  GetPostResponse({required this.statusCode, required this.body});

  factory GetPostResponse.build(int statusCode, Post postResponse) {
    return GetPostResponse(statusCode: statusCode, body: postResponse);
  }
}
