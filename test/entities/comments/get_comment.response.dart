import '../../model/comment.model.dart';

class GetCommentResponse {
  int statusCode;
  Comment body;

  GetCommentResponse({required this.statusCode, required this.body});

  factory GetCommentResponse.build(int statusCode, Comment comment) {
    return GetCommentResponse(statusCode: statusCode, body: comment);
  }
}
