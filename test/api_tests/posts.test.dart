import 'package:flutter_test/flutter_test.dart';

import '../clients/post.client.dart';

void main() {
  group('Posts Tests', () {
    test('validate Status Code and Id of Get Posts', () async {
      const id = 1;
      final postResponse = await PostClient().getPost(id);
      expect(postResponse.statusCode, 200, reason: 'Expected status code should be 200');
      expect(postResponse.body.id, 1, reason: 'Expected ID should be $id');
    });
  });
}
