import 'package:flutter_test/flutter_test.dart';

import '../clients/comment.client.dart';

void main() {
  group('Comments Tests', () {
    test('validate Status Code and Id of Get Comment', () async {
      const id = 1;
      final commentResponse = await CommentClient().getComment(id);
      expect(commentResponse.statusCode, 200, reason: 'Expected status code should be 200');
      expect(commentResponse.body.id, 1, reason: 'Expected ID should be $id');
    });
  });
}
