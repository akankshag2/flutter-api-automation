class Urls {
  static const _base = 'https://jsonplaceholder.typicode.com';
  static const posts = '$_base/posts';
  static const comments = '$_base/comments';
}
