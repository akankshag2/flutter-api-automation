import 'dart:convert';

import 'package:http/http.dart';

import '../entities/posts/get_post.response.dart';
import '../model/post.model.dart';
import '../resources/endpoints.dart';

class PostClient {
  Future<GetPostResponse> getPost(int postId) async {
    final response = await get(Uri.parse("${Urls.posts}/$postId"));
    return GetPostResponse.build(response.statusCode, Post.fromJson(jsonDecode(response.body)));
  }
}
