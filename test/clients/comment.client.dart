import 'dart:convert';

import 'package:http/http.dart';

import '../entities/comments/get_comment.response.dart';
import '../model/comment.model.dart';
import '../resources/endpoints.dart';

class CommentClient {
  Future<GetCommentResponse> getComment(int commentId) async {
    final response = await get(Uri.parse("${Urls.comments}/$commentId"));
    return GetCommentResponse.build(response.statusCode, Comment.fromJson(jsonDecode(response.body)));
  }
}
