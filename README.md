## flutter_api_automation

Basic Flutter Setup with API tests.


### Code Formatting

Line width = 140 columns
```
dart format test -l 140
```

### Steps to run test

- To Run the tests
```
flutter test test/sanity.suite.dart --reporter=expanded
```

### References
- [Http](https://pub.dev/packages/http) - Http Client for Dart
- [Test](https://pub.dev/packages/test) - Test Package
- [Dart Pojo](https://app.quicktype.io/?l=dart) - Online Pojo Creation
    

